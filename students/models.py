from django.db import models

# Create your models here.


class Student(models.Model):
    name = models.CharField(max_length=20)
    batch_no = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.name} {self.batch_no}"
