from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, name, batch_number, passing_year, phone_number, **extra_fields):
        user = self.model(name=name, batch_number=batch_number, passing_year=passing_year, phone_number=phone_number,
                          **extra_fields)
        password = name+batch_number  # generating password by concat name and batch_number
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone_number, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_voter', False)
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user