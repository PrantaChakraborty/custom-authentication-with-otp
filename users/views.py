from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView
from .models import CustomUser, Code
from students.models import Student
from .forms import RegisterAndLoginForm, CodeForm

# Create your views here.

"""
function for check student existence in table
"""


def student_identity_check(name, batch_number):
    try:
        return Student.objects.get(name=name, batch_no=batch_number)
    except Student.DoesNotExist:
        return None


class Home(TemplateView):
    template_name = 'home.html'


def register_and_login(request):
    form = RegisterAndLoginForm()
    if request.method == 'POST':
        form = RegisterAndLoginForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            batch_number = form.cleaned_data['batch_number']
            passing_year = form.cleaned_data['passing_year']
            phone_number = form.cleaned_data['phone_number']
            password = name + batch_number  # generating password

            """
            checking a user is already exist 
            """
            if CustomUser.objects.filter(phone_number=phone_number).exists():
                # print('executed')

                user = authenticate(request, phone_number=phone_number, password=password)
                if user is not None:
                    #login(request, user)
                    # print('logged in')
                    request.session['pk'] = user.pk
                    return redirect('otp_verify')
                else:
                    print('login failed')
                    return None
            else:
                """
                checking the student name and batch id are in student table or not
                """
                student_exist_in_table = student_identity_check(name, batch_number)
                if student_exist_in_table:
                    """
                    creating user
                    """
                    CustomUser.objects.create_user(name=name, batch_number=batch_number, passing_year=passing_year,
                                                   phone_number=phone_number)
                    # print(phone_number)
                    """
                    login user
                    """
                    user = authenticate(request, phone_number=phone_number, password=password)
                    if user is not None:
                        # login(request, user)
                        # return redirect('home')
                        request.session['pk'] = user.pk
                        return redirect('otp_verify')
                    else:
                        print('login failed')
                        return None
    return render(request, 'registration.html', {'form': form})


def otp_verify(request):
    form = CodeForm(request.POST or None)
    pk = request.session.get('pk')
    if pk:
        user = CustomUser.objects.get(pk=pk)
        code = user.code
        """
        printing the otp code in console
        """
        print("otp code is :", code)
        if form.is_valid():
            number = form.cleaned_data['number']
            if str(code) == number:
                code.save()
                login(request, user)
                return redirect('home')
            else:
                return redirect('register')
    return render(request, 'verify.html', {'form': form})




def logout_view(request):
    logout(request)
    return redirect('home')



