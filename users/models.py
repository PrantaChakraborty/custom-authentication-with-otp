from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager

import random
# Create your models here.


class CustomUser(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=50)
    batch_number = models.CharField(max_length=20)
    passing_year = models.PositiveIntegerField(max_length=4)
    phone_number = models.CharField(max_length=11, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_voter = models.BooleanField(default=True)

    objects = UserManager()
    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = ['name', 'batch_number', 'passing_year', ]

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return f"{self.name} {self.batch_number} {self.passing_year}"


class Code(models.Model):
    otp_code = models.CharField(max_length=5, blank=True)
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Codes'

    def __str__(self):
        return str(self.otp_code)

    def save(self, *args, **kwargs):
        number_list = [num for num in range(10)]
        code_items = []

        for i in range(5):
            number = random.choice(number_list)
            code_items.append(number)
        code_string = "".join(str(item) for item in code_items)
        self.otp_code = code_string
        super().save(*args, **kwargs)