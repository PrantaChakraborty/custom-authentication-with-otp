from .models import Code
from django import forms


class RegisterAndLoginForm(forms.Form):
    name = forms.CharField(max_length=50, label='Name')
    batch_number = forms.CharField(max_length=20, label='Batch number')
    passing_year = forms.CharField(max_length=4, label='Passing year')
    phone_number = forms.CharField(max_length=11, label='Phone number')


class CodeForm(forms.Form):
    number = forms.CharField(label='Code', help_text='Enter SMS verfication code')