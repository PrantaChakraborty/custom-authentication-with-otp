from django.urls import path
from .views import Home, register_and_login, logout_view, otp_verify
urlpatterns = [
    path('register/', register_and_login, name='register'),
    path('', Home.as_view(), name='home'),
    path('verify/', otp_verify, name='otp_verify'),
    path('logout/', logout_view, name='logout'),
]